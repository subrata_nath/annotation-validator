package net.therap.validator.service;

import net.therap.validator.annotation.Size;
import net.therap.validator.model.ValidationError;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author subrata
 * @since 11/8/16
 */
public class AnnotatedValidator {

    public static void print(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            System.out.println(error.getErrorMsg());
        }
    }

    public static List<ValidationError> validate(Object targetObject) throws IllegalAccessException,
            NoSuchFieldException {
        Field[] fields = targetObject.getClass().getDeclaredFields();
        List<ValidationError> errorList = new ArrayList<>();

        for (Field field : fields) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(Size.class)) {
                Size annotation = field.getAnnotation(Size.class);
                int min = annotation.min();
                int max = annotation.max();

                if (field.getType().getSimpleName().equals("String")) {

                    if (((String) field.get(targetObject)).length() < min ||
                            ((String) field.get(targetObject)).length() > max) {

                        errorList.add(generateError(annotation, field, min, max));
                    }

                } else if (field.getType().getSimpleName().equals("int")) {

                    if ((int) field.get(targetObject) < min) {

                        errorList.add(generateError(annotation, field, min, max));
                    }
                }
            }
        }

        return errorList;
    }

    private static ValidationError generateError(Size annotation, Field field, int min, int max) {
        String message = annotation.message();
        ValidationError error = new ValidationError();
        message = message.replaceAll("\\{min\\}", min + "").replaceAll("\\{max\\}", max + "");
        error.setFieldName(field.getName());
        error.setFieldType(field.getType().getSimpleName());
        error.setErrorMsg(message);
        return error;
    }
}
