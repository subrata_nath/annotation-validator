package net.therap.validator.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author subrata
 * @since 11/7/16
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface Size {
    int min() default 1;

    int max() default 100;

    String message() default "Length must be {min}-{max}";
}
