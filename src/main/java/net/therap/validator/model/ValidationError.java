package net.therap.validator.model;

/**
 * @author subrata
 * @since 11/8/16
 */
public class ValidationError {

    private String errorMsg;

    private String fieldType;

    private String fieldName;

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = fieldName + "(" + fieldType + ")" + ": " + errorMsg;
    }
}
