package net.therap.validator.controller;

import net.therap.validator.model.Person;
import net.therap.validator.model.ValidationError;
import net.therap.validator.service.AnnotatedValidator;

import java.util.List;

/**
 * @author subrata
 * @since 11/7/16
 */
public class Main {

    public static void main(String[] args) throws IllegalAccessException, NoSuchFieldException {
        Person p = new Person("Subrata Nath", 0);
        List<ValidationError> errors = AnnotatedValidator.validate(p);
        AnnotatedValidator.print(errors);

        Person p1 = new Person("Subrata Nath", 120);
        List<ValidationError> errors2 = AnnotatedValidator.validate(p1);
        AnnotatedValidator.print(errors2);
    }
}
